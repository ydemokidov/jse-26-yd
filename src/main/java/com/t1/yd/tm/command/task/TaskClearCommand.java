package com.t1.yd.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_clear";

    @NotNull
    public static final String DESCRIPTION = "Clear tasks";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
