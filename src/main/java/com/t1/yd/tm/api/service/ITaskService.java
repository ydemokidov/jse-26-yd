package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@NotNull String name);

    @NotNull
    Task create(@NotNull String name, @NotNull String description);

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    Task findTaskById(@NotNull String userId, @NotNull String id);

    Task findTaskByIndex(@NotNull String userId, @NotNull Integer index);

    Task removeTaskById(@NotNull String userId, @NotNull String id);

    Task removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

}
