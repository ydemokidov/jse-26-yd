package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.exception.field.NameEmptyException;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        if (argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty()) throw new NameEmptyException();
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
